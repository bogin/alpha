<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Ticket;
use App\Models\EditableText;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewTicket;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:tickets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Emails all unsent tickets to admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tickets = Ticket::whereNull("mail_sent")->get();
        $adminEmail = EditableText::where('name', 'admin_email')->first()->content;

        foreach ($tickets as $ticket) {
            if (Mail::to($adminEmail)->send(new newTicket($ticket))) {
                $ticket->mail_sent = Carbon\Carbon::now();
                $ticket->save();
            }
        }
    }
}
