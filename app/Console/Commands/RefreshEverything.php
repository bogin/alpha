<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RefreshEverything extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 're:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears all cache, rebuilds database, reseeds it and clears cache again just for kicks.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Refreshing migrations...');
        $this->call('migrate:refresh');

        $this->comment('Seeding database...');
        $this->call('db:seed');

        $this->comment('Clearing config cache...');
        $this->call('config:clear');

        $this->comment('Clearing cache...');
        $this->call('cache:clear');

        $this->comment('Clearing compiled...');
        $this->call('clear-compiled');

        $this->comment('Clearing route cache...');
        $this->call('route:cache');

        $this->comment('Clearing view cache...');
        $this->call('view:clear');

        $this->comment('Dumping autoload files...');
        exec('composer dump-autoload');

        $this->comment('All done! :)');
    }
}
