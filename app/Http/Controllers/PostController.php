<?php

namespace App\Http\Controllers;

use App\Post;
use App\Tag;
use Illuminate\Http\Request;

// use Illuminate\Support\Facades\Facade;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  String  return_statment
     * @return \Illuminate\Http\Response
     */
    public function index($return_statment = null)
    {
      $posts = Post::all();
      return view('posts.index', compact('posts'))->with('return_statment',$return_statment);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('posts.create');
    }

    /**
     * when a store/update request come in , that function
     * been called. its update the $post-tags in db
     * @param  \Illuminate\Http\Request  $request
     * @param  String  return_statment
     * @param  Post $post
     * @return Array [ "tags" => $new_tags, "return_statment" => $return_statment ]
     */
    function makeTagList($request , $return_statment, $post){
      // expected input
      // "[{"name":"fisrt name"},{"name":"seconed name "},{"name":"1"}]"
        $tags = json_decode(stripslashes($request->input('tags_list')));
      //expected output
      // array:3 [▼
      //   0 => {#509 ▼
      //     +"name": "fisrt name"
      //   }
      //   1 => {#510 ▼
      //     +"name": "seconed name "
      //   }
      //   2 => {#511 ▼
      //     +"name": "1"
      //   }
      // ]
      $new_tags = [];

      if(!is_array($tags)) // if tag list is not a list
        return back()->with('success', 'Post has bad tags list');

      foreach ($tags as $tag) {
          $name = $tag->name;

          $by = (is_numeric($name)) ? 'id' : 'name';

          $tmp = Tag::where($by , '=',  $name )->get();

          if(is_numeric($name)){

                if(!$tmp->count()){ // if there no tag with the specified id
                  $return_statment = $return_statment.', but tag with id '.$name.' is not exist. ';
                }

                else{
                  // add tag with the given id only if it isnt on post tag list all ready
                  if(!$this->hasTag($post->tags , $tmp[0]->id)){
                      $post->tags()->attach($tmp[0]->id);
                  }
                }
          }
          else{
            // if there no tag with the specified name create and attach it to post
            if(!$tmp->count()) {
               $new = Tag::create(json_decode( json_encode($tag), true));
               $post->tags()->attach($new->id);
             }
             else{
                 // add tag with the given name only if it isnt on post tag list all ready
               if(!$this->hasTag($post->tags , $tmp[0]->id)){
                   $post->tags()->attach($tmp[0]->id);
               }
             }
          }
       }
       return [
         "tags" => $new_tags,
         "return_statment" => $return_statment
       ];
    }



    /**
     * check id tags has tag with given id
     * @param  Array  tags
     * @param  String id
     * @return Boolean
     */
    function hasTag($tags , $id){
      foreach ($tags as $tag) {
        if($tag->id == $id)
         return true;
      }
      return false;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $return_statment = 'Post has been added';
      $this->validate(request(), [
       'post_content' => 'required|string',
       'tags_list' => 'string'
     ]);
     $new_post['post_content'] = $request->input('post_content');
     $post = Post::create($new_post);
     //save relevent tags to db
     $tagPreper = $this->makeTagList($request, $return_statment, $post);
     $posts = Post::all();
     return view('posts.index', compact('posts'))->with('return_statment', $tagPreper['return_statment']);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $post = Post::find($id);
      return view('posts.edit',compact('post','id'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $return_statment = 'Post has been Updated';
      $post = Post::find($id);
      $this->validate(request(), [
        'post_content' => 'required',
        'tags_list' => 'string'
      ]);
      //save relevent tags to db
      $tagPreper = $this->makeTagList($request, $return_statment , $post);
      $post->post_content = $request->input('post_content');
      $post->save();

      $posts = Post::all();
      return view('posts.index', compact('posts'))->with('return_statment', $tagPreper['return_statment']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $post = Post::find($id);
      foreach ($post->tags as $tag) {
        $tag->posts()->detach($post->id);
      }
      $post->delete();
      $posts = Post::all();
      return view('posts.index', compact('posts'))->with('return_statment','Post has been added');
    }
}
