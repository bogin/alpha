<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($return_statment = null)
    {
      $tags = Tag::all()->toArray();
      return view('tags.index', compact('tags'))->with('return_statment',$return_statment);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $tag = $this->validate(request(), [
       'name' => 'required',
     ]);

     Tag::create($tag);

     return back()->with('success', 'Tag has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
      $tag = Tag::find($id);
      return view('tags.edit',compact('tag','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $return_statment = 'Tag has been Updated';
      $this->validate(request(), [
        'name' => 'required'
      ]);
      $tag = Tag::find($id)->update(['name' => $request->input('name')]);
      return $this->index($return_statment);
    }

    /**
     * show all post og tag
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $tag = Tag::find($id);
      $posts = $tag->posts->toArray();
      $name = $tag->name;
      return view('tags.postsList',compact('tag','id','name','posts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $tag = Tag::find($id);
      if($tag->posts){
        foreach ($tag->posts as $post) {
          $post->tags()->detach($tag->id);
        }
      }
      $tag->delete();

      return $this->index('Tag has been deleted');
    }
}
