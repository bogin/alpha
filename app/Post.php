<?php

namespace App;
//
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
  protected $fillable = ['post_content','tags_list'];

  /**
    * Get the post record associated with the tag.
    */
   public function tags()
   {
       return $this->belongsToMany('App\Tag');
   }

}
