<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
  // Schema::table('posts', function (Blueprint $table) {
  //       $table->dropForeign('tags');
  //       $table->dropColumn(['tags']);
  //
  // });
  // Schema::table('posts', function (Blueprint $table) {
  //       $table->dropForeign('tags_list');
  //       $table->dropColumn(['tags_list']);
  //
  // });
  //
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('post_content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::drop('posts');
        Schema::dropIfExists('posts');
    }
}
