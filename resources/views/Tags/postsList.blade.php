<!-- postsList.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Post of Tag {{$name}}</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <br />
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Content</th>
        <th>Edit Post</th>
        <!-- <th>Delete Me From Post</th> -->
      </tr>
    </thead>
    <tbody>
      @foreach($posts as $post)
      <tr>
        <td>{{$post['id']}}</td>
        <td>{{$post['post_content']}}</td>
        <td><a href="{{action('PostController@edit', $post['id'])}}" class="btn btn-warning">Edit Post</a></td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  </body>
</html>
