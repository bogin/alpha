<!-- create.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>create</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <!-- Custom styles for this template -->
    <link href="{{asset('starter-template.css')}}" rel="stylesheet">
    <link href="{{asset('style.css')}}" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <h2>Create A Tag</h2><br  />
      <!-- if the data sent on create in not valid -->
      @if ($errors->any())
      <div class="alert alert-danger">
         <ul>
             @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
             @endforeach
         </ul>
       </div><br />
       @endif
       <!-- if the return status of create request return success -->
       @if (\Session::has('success'))
       <div class="alert alert-success">
           <p>{{ \Session::get('success') }}</p>
       </div><br />
       @endif

      <form method="post" action="{{url('tags')}}">
        {{csrf_field()}}
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="name">Name:</label>
            <input type="text" id="search" class="form-control" name="tag">
          </div>
        </div>

        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Add Tag</button>
          </div>
        </div>
      </form>
    </div>



  </body>
</html>
