<!-- index.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
    <br />

    <div id="statment_d" class="alert alert-success">
        <p id="statment_p">{{$return_statment}}</p>
    </div><br />

    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Edit</th>
        <th>Show My Posts</th>
        <th>DELETE</th>
      </tr>
    </thead>
    <tbody>
      @foreach($tags as $tag)
      <tr>
        <td>{{$tag['id']}}</td>
        <td>{{$tag['name']}}</td>
        <td><a href="{{action('TagController@edit', $tag['id'])}}" class="btn btn-warning">Edit</a></td>
        <td><a href="{{action('TagController@show', $tag['id'])}}" class="btn btn-warning">Show Posts</a></td>
        <td>
          <form action="{{action('TagController@destroy', $tag['id'])}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script type="text/javascript" src="{{ asset('js/select.js') }}"></script>
  </body>
</html>
