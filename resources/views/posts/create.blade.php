<!-- create.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>create</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Create A Post</h2><br  />
      <!-- if the data sent on create in not valid -->
      @if ($errors->any())
     <div class="alert alert-danger">
         <ul>
             @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
             @endforeach
         </ul>
     </div><br />
     @endif
     <!-- if the return status of create request return success -->
     @if (\Session::has('success'))
     <div class="alert alert-success">
         <p>{{ \Session::get('success') }}</p>
     </div><br />
     @endif

      <form method="post" action="{{url('posts')}}" enctype='application/json'>
        {{csrf_field()}}
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
          <label for="post_content">Say SomeThing</label>
            <input type="text" class="form-control" id="post_content" name="post_content" >
          </div>
        </div>
        <!-- input tag name and add to list button -->
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <input type="hidden" id="tags_list" class="form-control" name="tags_list" >
            <label for="tags">Tag Name</label>
            <input type="text" id="tags" class="form-control" name="tags"></br>
            <button type="button" id="addTagBtn" class="btn btn-success">Add To List</button>
          </div>
        </div>
        <!-- display table -->
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Name</th>
                </tr>
            </thead>
            <tbody class="tag_table">
            </tbody>
            </table>
          </div>
        </div>
        <!-- send button -->
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Add Post </button>
          </div>
        </div>
      </form>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/select.js') }}"></script>

  </body>
</html>
