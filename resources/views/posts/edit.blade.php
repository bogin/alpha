<!-- edit.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Edit Post {{$id}} </title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  </head>
  <body>
    <div class="container">
        <h2>Edit A Post</h2><br/>
          <!-- if the data sent on create in not valid -->
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div><br />
        @endif
        <form method="post" action="{{action('PostController@update', $id)}}">
          {{csrf_field()}}
          <input name="_method" type="hidden" value="PATCH">
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="post_content">Post Content:</label>
              <input type="text" class="form-control" name="post_content" value="{{$post->post_content}}">
            </div>
          </div>
          <!-- add name tag -->
          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <input type="hidden" id="tags_list" class="form-control" name="tags_list" >
              <input type="text" id="tags_edit" class="form-control" name="tags">
              <button type="button" id="addTagEditPage" class="btn btn-success">Add Tags</button>
            </div>
          </div>
          <!-- tags table -->
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Id</th>
              </tr>
            </thead>
            <!-- display post tags array -->
            <tbody class="tag_table">
              @foreach($post->tags->toArray() as $tag)
              <tr>
                  <td class="tag_name">{{$tag['name']}}</td>
                  <td class="tag_name">{{$tag['id']}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>

          <div class="row">
            <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <button type="submit" class="btn btn-success" style="margin-left:38px">Update Post</button>
            </div>
        </div>
      </form>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script type="text/javascript" src="{{ asset('js/select.js') }}"></script>
    </div>
  </body>
</html>
