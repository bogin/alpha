<!-- index.blade.php -->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Index Page</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>All Posts</h2><br  />
    <!-- if the data sent on create in not valid -->
      @if ($errors->any())
      <div class="alert alert-danger">
         <ul>
             @foreach ($errors->all() as $error)
                 <li>{{ $error }}</li>
             @endforeach
         </ul>
       </div><br />
       @endif
       <!-- display massage -->
       <div id="statment_d" class="alert alert-success">
           <p id="statment_p">{{$return_statment}}</p>
       </div><br />

        <table class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>POST CONTENT</th>
            <th>TAG NAME</th>
            <th>TAG ID</th>
            <th>Edit</th>
            <th>DELETE</th>
          </tr>
        </thead>
        <tbody>
          <!-- each post as a row -->
          @foreach($posts as $post)
          <tr>
            <td>{{$post['id']}}</td>
            <td>{{$post['post_content']}}</td>
            <td>
                @foreach($post->tags as $tag)
                <div class="tagCell">   {{$tag['name']}}   </div>
                @endforeach
            </td>
            <td>
              <a href="{{action('PostController@edit', $post['id'])}}" class="btn btn-warning">Edit</a>
            </td>
             <td>
               <form action="{{action('PostController@destroy', $post['id'])}}" method="post">
                 {{csrf_field()}}
                 <input name="_method" type="hidden" value="DELETE">
                 <button class="btn btn-danger" type="submit">Delete</button>
               </form>
             </td>
           </tr>
          @endforeach
        </tbody>
      </table>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script type="text/javascript" src="{{ asset('js/select.js') }}"></script>
  </body>
</html>
