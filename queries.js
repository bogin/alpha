var promise = require('bluebird');
var options = {
  promiseLib: promise
};
var fs = require('fs');
var pgp = require('pg-promise')(options);
// var connectionString = 'postgres://localhost:5432/Revenues';
// var db = pgp(connectionString);
var db = pgp({
    host: 'localhost',
    port: 5432,
    database: 'revenuesdb',
    user: 'postgres',
    password: 'root'
});
function getSingleRevenue(req, res, next) {

  var user_id = parseInt(req.params.user_id);
  db.one('select * from Revenues where user_id = $1', user_id)
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ONE puppy'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}

function getAllRevenues(req, res, next) {
  db.any('select * from Revenues')
    .then(function (data) {
      res.status(200)
        .json({
          status: 'success',
          data: data,
          message: 'Retrieved ALL Revenues'
        });
    })
    .catch(function (err) {
      return next(err);
    });
}
function liveEvent(req, res, next) {
   var user_id = parseInt(req.body.user_id),
       value = parseInt(req.body.value),
       name = req.body.name,
       obj;
       if (fs.existsSync('foo.json')) {
           console.log('Found file');
           fs.readFile('foo.json', 'utf8', function readFileCallback(err, data){
                if (err){
                    console.log(err);
                } else {
                obj = JSON.parse(data); //now it an object
                console.log("obj: ");
                console.log(obj);
                obj.table.push({user_id: user_id, name:name , value:value}); //add some data
                json = JSON.stringify(obj); //convert it back to json
                fs.writeFile('foo.json', json, 'utf8', function(error){console.log(error)}); // write it back
            }});
       }
       else{
         obj = {
             table: []
          };
          obj.table.push({user_id: user_id, name:name , value:value}); //add some data
          json = JSON.stringify(obj); //convert it back to json
          fs.writeFile('foo.json', json, 'utf8', callback); // write it back
         console.log('not Found file');
       }
       res.status(200)
             .json({
               status: 'success',
               message: 'Inserted one puppy'
             });

}

// add query functions

module.exports = {
  // liveEvent: liveEvent,
  // userEvents: userEvents,
  getAllRevenues: getAllRevenues,
  getSingleRevenue: getSingleRevenue,
  liveEvent: liveEvent,
  // updateRevenue: updateRevenue,
  // removeRevenue: removeRevenue
};
